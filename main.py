from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.storage.jsonstore import JsonStore


class ToDoListApp(App):
    def build(self):
        # Charger les tâches existantes depuis le JsonStore
        self.store = JsonStore('tasks.json')
        if self.store.exists('tasks'):
            self.tasks = self.store.get('tasks')['tasks']
        else:
            self.tasks = []

        # Interface utilisateur
        self.layout = BoxLayout(orientation='vertical')
        self.task_input = TextInput(hint_text='Nouvelle tâche', multiline=False)
        self.add_button = Button(text='Ajouter', on_press=self.add_task)
        self.task_list = BoxLayout(orientation='vertical')

        # Ajouter les composants à la mise en page
        self.layout.add_widget(self.task_input)
        self.layout.add_widget(self.add_button)
        self.layout.add_widget(self.task_list)

        # Afficher les tâches existantes
        self.load_tasks()

        return self.layout

    def add_task(self, instance):
        new_task = self.task_input.text.strip()
        if new_task:
            self.tasks.append({'task': new_task, 'completed': False})
            self.update_store()
            self.load_tasks()
            self.task_input.text = ''  # Effacer le champ de saisie après l'ajout

    def remove_task(self, task):
        self.tasks = [t for t in self.tasks if t['task'] != task['task']]
        self.update_store()
        self.load_tasks()

    def toggle_task_completion(self, task):
        task['completed'] = not task['completed']
        self.update_store()
        self.load_tasks()

    def update_store(self):
        # Mettre à jour le JsonStore avec la liste mise à jour des tâches
        self.store.put('tasks', tasks=self.tasks)

    def load_tasks(self):
        # Effacer la liste actuelle des tâches
        self.task_list.clear_widgets()

        # Ajouter les tâches à la liste
        for task in self.tasks:
            task_layout = BoxLayout(orientation='horizontal')

            # Ajouter un bouton pour marquer la tâche comme complétée ou non
            toggle_button_text = 'Non finie' if not task['completed'] else 'Finie !'
            toggle_button = Button(text=toggle_button_text,
                                   on_press=lambda instance, task=task: self.toggle_task_completion(task))
            task_layout.add_widget(toggle_button)

            # Ajouter le libellé de la tâche
            task_label = Button(text=task['task'], disabled=task['completed'])
            task_layout.add_widget(task_label)

            # Ajouter le bouton de suppression
            remove_button = Button(text='Supprimer', on_press=lambda instance, task=task: self.remove_task(task))
            task_layout.add_widget(remove_button)

            self.task_list.add_widget(task_layout)


if __name__ == '__main__':
    ToDoListApp().run()
